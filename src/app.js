import express from 'express';
import { createServer } from 'http';
import { ApolloServer } from 'apollo-server-express';
import bodyParser from "body-parser";
import cors from 'cors';

import { PORT } from './config';
import connectDB from './helpers/connectDB';
import resolvers from './schema/resolvers';
import typeDefs from './schema/typeDefs';

connectDB();
const app = express();
app.use(bodyParser.json());

app.use(cors());

app.get('/', (req, res) => res.json({ data: 'Endpoint great!' }));

app.use('/uploads', express.static('uploads'));

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req, connection }) => ({ req, connection }),
});

apolloServer.applyMiddleware({ app });

const httpServer = createServer(app);
apolloServer.installSubscriptionHandlers(httpServer);

app.use((req, res, next) => {
  const error = new Error('Not Found!!');
  error.status = 404;
  next(error);
});

app.use((error, req, res) => {
  res.status(error.status || 500);
  res.json({ error: { msg: error.message } });
});

httpServer.listen({ port: PORT }, () => {
  console.log(`🚀 Server ready at http://localhost:${PORT}${apolloServer.graphqlPath}`);
  const subPath = apolloServer.subscriptionsPath;
  console.log(`Subscriptions are at ws://localhost:${PORT}${subPath}`);
});
