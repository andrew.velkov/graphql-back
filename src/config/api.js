import 'dotenv/config';

module.exports = {
  PORT: process.env.PORT || 5003,
  JWT: {
    access: {
      secret: process.env.JWT_SECRET_ACCESS,
      type: 'access',
      exp: '30d',
    },
    refresh: {
      secret: process.env.JWT_SECRET_REFRESH,
      type: 'refresh',
      exp: '30d',
    },
  },
}
