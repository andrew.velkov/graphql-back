import { AuthenticationError } from 'apollo-server-express';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';

import { JWT } from '../config/api';
import UserModel from '../models/UserModel';

module.exports = async (context) => {
  const { req, connection } = context;
  
  // get token from header
  let headerAuth;

  if (connection) {
    headerAuth = await context.connection.context.headers
      ? context.connection.context.headers.Authorization
      : context.connection.context.Authorization;
  } else {
    headerAuth = req.headers.authorization;
  }

  // check if not token
  if (!headerAuth) {
    throw new AuthenticationError('No token, authorization denied');
  }

  // verify token
  try {
    const token = headerAuth.replace('Bearer ', '');
    const user = jwt.verify(token, JWT.access.secret);
    if (user.type !== JWT.access.type) {
      throw new AuthenticationError('Unauthorized access');
    }

    const isValidateUserId = mongoose.Types.ObjectId.isValid(user.userId);
    if (!isValidateUserId) {
      throw new AuthenticationError('Invalid user ID');
    };

    const isUserId = await UserModel.findOne({ _id: user.userId });
    if (!isUserId.id) {
      throw new AuthenticationError('Such user does not exis');
    }

    return { userId: user.userId, req, connection };
    // return { userId: '11', req, connection };
  } catch (err) {
    throw new AuthenticationError('Token is not valid');
  }
}
