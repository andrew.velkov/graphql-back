import mongoose from 'mongoose';
// mongoose.Promise = require('bluebird');
// import { PubSub } from 'graphql-subscriptions';
// import async from 'async';

import { BOTS } from '../../config';
import auth from '../../middleware/auth';
import UserModel from '../../models/UserModel';
import ChatModel from '../../models/ChatModel';
import ChatMessageModel from '../../models/ChatMessageModel';

const getChatGroup = async (parent, args, context) => {
  const { userId } = await auth(context);

  await UserModel.findOneAndUpdate({ _id: userId }, { $set: {
    "last_seen" : new Date().toISOString(),
  } }).select(['-__v', '-password']);

  let populateForUser = ['first_name', 'last_name', 'profile_img', 'last_seen'];

  return await ChatModel.find({
    $and: [
      { $or: [{ sender: userId }, { receiver: userId }] },
    ],
  }).populate('receiver', [...populateForUser])
    .populate('sender', [...populateForUser])
    .skip(args.offset)
    .limit(25)
    .exec()
    .then(async (conversations) => {
      if (!conversations) {
        return new Error('Conversations error');
      }

      // add field "user" receiver
      conversations.forEach(async (item) => {
        // bots
        const bot = BOTS.find((bot) => bot.name === item.chat_bot);
        if (item.receiver === null && bot.id) {
          Object.assign(item, { receiver: {
            _id: bot.id,
            first_name: bot.value,
            last_name: 'bot',
            profile_img: bot.image,
            last_seen: new Date().toISOString(),
          } });
        }
    
        switch(userId) {
          case String(item.receiver._id):
            return Object.assign(item, { user: item.sender });
          case String(item.sender._id):
            return Object.assign(item, { user: item.receiver });
        }
      });
      // add field "user" receiver

      let dialogs = conversations.map(async dialog => {
        const message = await ChatMessageModel.findOne({
          chat_id: dialog._id,
          $and: [
            { $or: [{ sender: userId }, { receiver: userId }] },
            { $or: [{ sender: dialog.user._id }, { receiver: dialog.user._id }] },
          ],
        }).lean().select(['-attached', '-updatedAt', '-__v']).sort({ createdAt: -1 });

        let unreadMessage = await ChatMessageModel.find({
            chat_id: dialog._id,
            is_read: false,
            receiver: userId,
          }).lean().sort({ createdAt: -1 });

        Object.assign(dialog, { last_message: message || {} }, { unread_messages: unreadMessage.length });
        return dialog;
      });

      return Promise.all(dialogs);
    }).then((dialogs) => dialogs).catch((err) => new Error(err));
};

const getChatMessage = async (parent, args, context) => {
  const { userId } = await auth(context);
  const { chatId, offset } = args;
  let messages;

  try {
    messages = await ChatMessageModel.find({
      chat_id: chatId,
      $or: [{sender: userId }, {receiver: userId }],
    }).skip(offset).limit(10).sort({ createdAt: -1 })
  } catch (err) {
    return {
      error: err.codeName
    };
  }

  if (!messages) {
    return new Error('Messages error');
  }

  return messages.reverse();
};

const createChat = async (_, args, context) => {
  const { userId } = await auth(context);
  const { receiver } = args;

  if (userId === receiver) {
    return new Error('You cannot create a chat with yourself!');
  };

  const isValidReceiverId = mongoose.Types.ObjectId.isValid(receiver);
  if (!isValidReceiverId) {
    return new Error('ID is not validation');
  };

  const isChat = await ChatModel.findOne({
    $and: [
      { $or: [{ sender: userId }, { receiver: userId }] },
      { $or: [{ sender: receiver }, { receiver }] },
    ],
  });

  if (isChat) {
    return isChat;
  }

  const chatData = new ChatModel({
    sender: userId,
    receiver,
    chat_pin: [],
    // chat_archived: [],
    // chat_deleted: "",
    // chat_rename: [],
    // chat_blocked: []
  });

  const bot = BOTS.find((bot) => bot.id === receiver);
  if (bot && bot.id === receiver) chatData.chat_bot = bot.name;

  try {
    const createdChatGroup = await chatData.save();
    return createdChatGroup;
  } catch (err) {
    return {
      error: err.codeName
    };
  }
};

export {
  getChatGroup,
  getChatMessage,
  createChat,
};
