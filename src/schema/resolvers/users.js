import bcrypt from 'bcryptjs';

import generateToken from '../../helpers/generateToken';
import auth from '../../middleware/auth';
import UserModel from '../../models/UserModel';

// mutaion: login
const login = async (_, args, { res }) => {
  const { email, password } = args;
  const currentUser = await UserModel.findOne({ email }).select(['_id', 'password']);
  if (!currentUser) {
    return new Error('Invalid credentials');
  }
  const isMatch = await bcrypt.compare(password, currentUser.password);
  if (!isMatch) {
    return new Error('Invalid Credentials');
  }
  const { accessToken, refreshToken } = generateToken(currentUser._id);
  return { accessToken, refreshToken };
};

// mutaion: register
const register = async (_, args) => {
  const { email, password, firstName, lastName } = args;
  const salt = await bcrypt.genSalt(10);

  const userData = new UserModel({
    email,
    first_name: firstName,
    last_name: lastName,
    profile_img: '',
    last_seen: new Date().toISOString(),
    password: await bcrypt.hash(password, salt),
  });

  const getUserByEmail = await UserModel.findOne({ email });
  if (getUserByEmail) {
    return new Error('User already exists');
  }

  const newUserData = await userData.save();
  const { accessToken, refreshToken } = generateToken(newUserData._id);
  return { accessToken, refreshToken };
};

// query: user
const user = async (parent, args, context) => {
  const { userId } = await auth(context);
  try {
    const user = await UserModel.findById({ _id: userId }).select(['-__v', '-password']);
    return user;
  } catch (err) {
    return {
      error: err.codeName
    };
  }
};

// query: users
const users = async (parent, args, context) => {
  await auth(context);

  try {
    const data = await UserModel.find();
    return data;
  } catch (err) {
    return err.codeName;
  }
};

export {
  user,
  users,
  login,
  register,
}