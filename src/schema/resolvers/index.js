import mongoose from 'mongoose';
import { PubSub, withFilter } from 'graphql-subscriptions';

import { BOTS } from '../../config';
import auth from '../../middleware/auth';
import ChatModel from '../../models/ChatModel';
import ChatMessageModel from '../../models/ChatMessageModel';

import { login, register, user, users } from './users';
import { getChatGroup, getChatMessage, createChat } from './chats';

const pubsub = new PubSub();

const resolvers = {
  Query: {
    user,
    users,
    getChatGroup,
    getChatMessage,
  },

  Mutation: {
    login,
    register,
    createChat,
    createChatMessage: async (_, { chatId, receiver, message }, context) => {
      const { userId } = await auth(context);

      if (userId === receiver) {
        return new Error('You cannot write to yourself in this chat!');
      }

      if (message.length >= 1000) {
        return new Error('Message length 1000');
      }

      const isValidChatId = mongoose.Types.ObjectId.isValid(chatId);
      const isValidReceiverId = mongoose.Types.ObjectId.isValid(receiver);

      if (!isValidReceiverId || !isValidChatId) {
        return new Error('ID is not validation');
      };

      const chatRoom = await ChatModel.findOne({
        _id: chatId,
        $and: [
          { $or: [{ sender: userId }, { receiver: userId }] },
          { $or: [{ sender: receiver }, { receiver }] },
        ],
      });

      if (!chatRoom || chatRoom.length === 0) {
        return new Error('Query params is not validation - chat group');
      }

      const newMessage = new ChatMessageModel({
        chat_id: chatId,
        sender: userId,
        receiver,
        message,
        is_read: false,
      });

      try {
        return await newMessage.save().then(async chatNewMessage => {
          pubsub.publish('SUBSCRIBE_CHAT_NEW_MESSAGE', { chatNewMessage, userId, chatId });

          const bot = BOTS.find(bot => bot.id === receiver);
          if (bot.id && (bot.name !== 'IGNORE_BOT')) {
            const newMessageBot = new ChatMessageModel({
              chat_id: chatId,
              sender: receiver,
              receiver: userId,
              message,
              is_read: false,
            });

            const sleep = timeout => new Promise(resolve => setTimeout(resolve, timeout));
            let timeout;

            if (bot.name === 'REVERSE_BOT') {
              newMessageBot.message = message.split('').reverse().join('');
              timeout = 3000;
              await sleep(timeout);
            }

            if (bot.name === 'SPAM_BOT') {
              newMessageBot.message = `random: ${ Math.random().toString(36).substr(2, 5)}`;
              timeout = (Math.floor(Math.random() * (120 - 10)) + 10) * 1000;
              await sleep(timeout);
            }

            await newMessageBot.save().then(chatNewMessageBot => {
              pubsub.publish('SUBSCRIBE_CHAT_NEW_MESSAGE', { chatNewMessage: chatNewMessageBot, userId, chatId });
              return chatNewMessageBot;
            });
          }

          return chatNewMessage;
        });
      } catch (err) {
        return {
          error: err.codeName
        };
      }
    },

    userTyping: async (_, { receiver }, context) => {
      const { userId } = await auth(context);
      pubsub.publish("SUBSCRIBE_USER_TYPPING", { userTypingSubscription: receiver, userId });
      return true;
    },

    readMessages: async (_, { chatId, receiver }, context) => {
      const { userId } = await auth(context);

      const isValidChatId = mongoose.Types.ObjectId.isValid(chatId);
      const isValidReceiverId = mongoose.Types.ObjectId.isValid(receiver);

      if (!isValidReceiverId || !isValidChatId) {
        return new Error('ID is not validation');
      };

      await ChatMessageModel.updateMany({
        chat_id: chatId,
        sender: receiver,
        receiver: userId,
      }, { is_read: true }).sort({ createdAt: -1 });
      pubsub.publish("SUBSCRIBE_READ_MESSAGES", { readMessagesSubscription: receiver, userId, chatId });
      return null;
    },
  },

  Subscription: {
    chatNewMessage: {
      subscribe: withFilter(() => pubsub.asyncIterator(['SUBSCRIBE_CHAT_NEW_MESSAGE']), async (payload, variables, context) => {
        await auth(context);
        return payload.chatId === variables.chatId;
      }),
    },
    readMessagesSubscription: {
      subscribe: withFilter(() => pubsub.asyncIterator(['SUBSCRIBE_READ_MESSAGES']), async (payload, variables, context) => {
        await auth(context);
        // return true;
        return payload.chatId === variables.chatId;
      }),
    },
    userTypingSubscription: {
      subscribe: withFilter(() => pubsub.asyncIterator(['SUBSCRIBE_USER_TYPPING']), (payload, variables) => {
        return payload.userId === variables.receiver;
      })
    }
  },
};

export default resolvers;
