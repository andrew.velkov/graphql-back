import { gql } from 'apollo-server-express';

const typeDefs = gql`
  scalar Date

  type User {
    id: ID
    email: String
    first_name: String
    last_name: String
    last_seen: Date
    profile_img: String
    createdAt: Date
  }

  type Token {
    accessToken: String
    refreshToken: String
  }

  type UserInfoForChat {
    _id: ID
    first_name: String
    last_name: String
    profile_img: String
    last_seen: Date
    createdAt: Date
  }

  type CompanyInfoForChat {
    _id: ID
    name: String
  }

  type ArrayWithId {
    _id: ID
  }

  type Attached {
    file: String
  }

  type ChatMessage {
    _id: ID
    chat_id: ID
    sender: ID
    receiver: ID
    message: String
    is_read: Boolean
    createdAt: Date
    # attached: [Attached]
  }

  type Chat {
    _id: ID
    sender: UserInfoForChat
    receiver: UserInfoForChat
    user: UserInfoForChat
    chat_bot: String
    unread_messages: Int
    createdAt: Date
    last_message: ChatMessage
    # chat_pin: [ArrayWithId]
    # chat_rename: [UserInfoForChat]
    # chat_archived: [ArrayWithId]
    # chat_blocked: [ArrayWithId]
    # chat_deleted: String
  }

  type Query {
    user: User
    users: [User]
    getChatGroup(offset: Int): [Chat]
    getChatMessage(chatId: ID, offset: Int): [ChatMessage]
  }

  type Mutation {
    login(email: String, password: String): Token
    register(email: String, password: String, firstName: String, lastName: String): Token
    createChat(receiver: ID): Chat
    readMessages(chatId: ID, receiver: ID): ChatMessage
    createChatMessage(chatId: ID, receiver: ID, message: String ): ChatMessage
    userTyping(receiver: String): Boolean
    # renameChat(chatId: ID, receiver: ID, firstName: String, lastName: String): Chat
    # blockedChat(chatId: ID, receiver: ID): Chat
    # archivedChat(chatId: ID, receiver: ID): Chat
    # pinChat(chatId: ID, receiver: ID): Chat
    # removeChat(chatId: ID, receiver: ID): Chat
  }

  type Subscription {
    chatNewMessage(chatId: ID, receiver: ID): ChatMessage
    readMessagesSubscription(chatId: ID, receiver: ID): ChatMessage
    userTypingSubscription(receiver: String): String
  }
`;

export default typeDefs;
