import mongoose from 'mongoose'

const url = process.env.DB_URI;

const connectDB = async () => {
  try {
    await mongoose.connect(url, {
      keepAlive: true,
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    });
  } catch (err) {
    console.log('..connected error', err.message);
    process.on(1);
  }
}

module.exports = connectDB;
