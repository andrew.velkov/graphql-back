import jwt from 'jsonwebtoken';

import { JWT } from '../config/api';

const generateToken = (value) => {
  const { access, refresh } = JWT;

  const accessToken = jwt.sign({ userId: value, type: access.type }, access.secret, {
    expiresIn: access.exp,
  });

  const refreshToken = jwt.sign({ userId: value, type: refresh.type }, refresh.secret, {
    expiresIn: refresh.exp,
  });

  return {
    accessToken, refreshToken,
  };
}

export default generateToken;
