import mongoose, { Schema } from 'mongoose';

const ChatMessageSchema = new Schema({
  chat_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'chats',
  },
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'chats',
  },
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'chats',
  },
  message: {
    type: String,
    minlength: 1,
    maxlength: 1000,
    trim: true,
  },
  is_read: {
    type: Boolean,
    default: true,
  },
  attached: {
    files: [
      {
        file: {
          type: String,
          maxlength: 2000,
          trim: true,
        }
      },
    ],
  },
  // product: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'product',
  // },
  data: {
    type: mongoose.Schema.Types.Mixed,
  },
}, {
  timestamps: true,
});

const ChatMessageModel = mongoose.model('chat_messages', ChatMessageSchema);

export default ChatMessageModel;