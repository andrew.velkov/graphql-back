import mongoose, { Schema } from 'mongoose';

const ChatSchema = new Schema({
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  chat_pin: [
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
      },
    }
  ],
  chat_archived: [
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
      },
    }
  ],
  chat_blocked: [
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
      },
    }
  ],
  chat_rename: [
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
      },
      first_name: {
        type: String,
        trim: true,
        maxlength: 200,
      },
      last_name: {
        type: String,
        trim: true,
        maxlength: 200,
      },
    },
  ],
  chat_bot: {
    type: String,
    trim: true,
  },
  chat_deleted: {
    type: String,
  },
  data: {
    type: mongoose.Schema.Types.Mixed,
  },
}, {
  timestamps: true,
});

const ChatModel = mongoose.model('chats', ChatSchema);

export default ChatModel;
