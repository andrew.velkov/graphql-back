import mongoose, { Schema } from 'mongoose';

const AuthorSchema = new Schema({
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  first_name: {
    type: String,
    min: 3,
    max: 255,
    trim: true,
    required: true,
  },
  last_name: {
    type: String,
    max: 100,
    trim: true,
  },
  comments: {
    type: String,
    trim: true,
    max: 255,
  },
  data: {
    type: mongoose.Schema.Types.Mixed,
  },
}, {
  timestamps: true,
});

const AuthorModel = mongoose.model('authors', AuthorSchema);

export default AuthorModel;