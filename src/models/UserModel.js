import mongoose, { Schema } from 'mongoose';

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    max: 255,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  password: {
    type: String,
    required: true,
    min: 8,
    max: 255,
  },
  first_name: {
    type: String,
    max: 255,
    trim: true,
    required: true,
  },
  last_name: {
    type: String,
    max: 255,
    trim: true,
    required: true,
  },
  profile_img: {
    type: String,
    trim: true,
    maxlength: 1000,
  },
  last_seen: {
    type: Date,
  },
  last_typed: {
    type: Date,
  },
  data: {
    type: mongoose.Schema.Types.Mixed,
  },
}, {
  timestamps: true,
});

const UserModel = mongoose.model('users', UserSchema);

export default UserModel;